// SPDX-License-Identifier: GPL-2.0
/*
 * Shakti UART Driver for console.
 * Copyright (c) 2021, IIT Madras. Shakti Processor Software Team.  All rights reserved.
 * 
 * Author : Venkatakrishnan Sutharsan <venkatakrishnan.sutharsan@gmail.com> 
 */

#include <linux/clk.h>
#include <linux/console.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/io.h>
#include <linux/irq.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_irq.h>
#include <linux/platform_device.h>
#include <linux/serial_core.h>
#include <linux/serial_reg.h>
#include <linux/slab.h>
#include <linux/tty.h>
#include <linux/tty_flip.h>

/*
 * Config macros
 */

/*
 * SHAKTI_SERIAL_MAX_PORTS: maximum number of UARTs on a device that can
 *                          host a serial console
 */
#define SHAKTI_SERIAL_MAX_PORTS			1

/*
 * SHAKTI_DEFAULT_BAUD_RATE: default baud rate that the driver should
 *                           configure itself to use
 */
#define SHAKTI_DEFAULT_BAUD_RATE		19200

/* SHAKTI_SERIAL_NAME: our driver's name that we pass to the operating system */
#define SHAKTI_SERIAL_NAME			"shakti-serial"

/* SHAKTI_TTY_PREFIX: tty name prefix for Shakti serial ports */
#define SHAKTI_TTY_PREFIX			"ttySHAKTI"

/* SHAKTI_TX_FIFO_DEPTH: depth of the TX FIFO (in bytes) */
#define SHAKTI_TX_FIFO_DEPTH			16

/* SHAKTI_RX_FIFO_DEPTH: depth of the TX FIFO (in bytes) */
#define SHAKTI_RX_FIFO_DEPTH			16

#if (SHAKTI_TX_FIFO_DEPTH != SHAKTI_RX_FIFO_DEPTH)
#error Driver does not support configurations with different TX, RX FIFO sizes
#endif

// #define SHAKTI_SPI_PINMUX_MASK				0xFFC03FFF
// #define SHAKTI_SPI_PINMUX_SET				0x154000

// #define UART0_BASE_ADDR 0x11300
// #define SHAKTI_DEFAULT_CLOCK 50000000
#define SHAKTI_PORT_TYPE0                    120

#define REG_BAUD	                        0x00
#define REG_TX		                        0x04
#define REG_RX		                        0x08
#define REG_STATUS	                        0x0C
#define REG_DELAY	                        0x10
#define REG_CONTROL	                        0x14
#define REG_INT_EN	                        0x18
#define REG_IQ_CYCLES	                    0x1C
#define REG_RX_THRES	                    0x20

#define UART_TX_FULL                        0x2
#define UART_RX_FULL                        0x8
#define STS_RX_NOT_EMPTY                    1 << 2
#define STS_TX_FULL 	                    1 << 1
#define STS_TX_EMPTY 	                    1 << 0

#define GPIO_ADDR                           0x00040100
#define SHAKTI_SPI_PINMUX_SET				0x5
#define PINMUX_CONFIGURE_REG 				0x41510
#define SHAKTI_SPI_PINMUX_MASK				0xFFC03FFF  //Clearing the SPI concerned bits

/**
 * shakti_serial_port - driver-specific data extension to struct uart_port
 * @port: struct uart_port embedded in this struct
 * @dev: struct device *
 * @ier: shadowed copy of the interrupt enable register
 * @clkin_rate: input clock to the UART IP block.
 * @baud_rate: UART serial line rate (e.g., 115200 baud)
 * @clk_notifier: clock rate change notifier for upstream clock changes
 *
 * Configuration data specific to this Shakti UART.
 */
struct shakti_serial_port {
	struct uart_port	port;
	struct device		*dev;
	unsigned char		ier;
	unsigned long		clkin_rate;
	unsigned long		baud_rate;
	struct clk		*clk;
	// struct notifier_block	clk_notifier;
};

/*
 * Structure container-of macros
 */

#define port_to_shakti_serial_port(p) (container_of((p), struct shakti_serial_port, port))

// #define notifier_to_shakti_serial_port(nb) (container_of((nb), struct shakti_serial_port, clk_notifier))

/*
 * Forward declarations
 */
static void shakti_serial_stop_tx(struct uart_port *port);

/*
 * Internal functions
 */

/**
 * shakti_early_writel() - write to a Shakti serial port register (early)
 * @port: pointer to a struct uart_port record
 * @offs: register address offset from the IP block base address
 * @v: value to write to the register
 *
 * Given a pointer @port to a struct uart_port record, write the value
 * @v to the IP block register address offset @offs.  This function is
 * intended for early console use.
 *
 * Context: Intended to be used only by the earlyconsole code.
 */
static void shakti_early_writel(u32 v, u16 offs, struct uart_port *port)
{
	// Check the register to be accessed and make the read as byte, word or long...
	// if (offs == REG_TX || offs == REG_RX)
	// 	writel_relaxed(v, port->membase + offs);
	// else
	if(offs == REG_BAUD || offs == REG_DELAY || offs == REG_CONTROL)
		writew_relaxed(v, port->membase + offs);
	else
		writeb_relaxed(v, port->membase + offs);
}

/**
 * shakti_early_readl() - read from a Shakti serial port register (early)
 * @port: pointer to a struct uart_port record
 * @offs: register address offset from the IP block base address
 *
 * Given a pointer @port to a struct uart_port record, read the
 * contents of the IP block register located at offset @offs from the
 * IP block base and return it.  This function is intended for early
 * console use.
 *
 * Context: Intended to be called only by the earlyconsole code or by
 *          __ssp_readl() or __ssp_writel() (in this driver)
 *
 * Returns: the register value read from the UART.
 */
static u32 shakti_early_readl(struct uart_port *port, u16 offs)
{
	// Check the register to be accessed and make the read as byte, word or long...
	// if (offs == REG_TX || offs == REG_RX)
	// 	return readl_relaxed(port->membase + offs);
	// else 
	if(offs == REG_BAUD || offs == REG_DELAY || offs == REG_CONTROL)
		return readw_relaxed(port->membase + offs);
	else
		return readb_relaxed(port->membase + offs);
}

/**
 * __ssp_readl() - read from a Shakti serial port register
 * @ssp: pointer to a struct shakti_serial_port record
 * @offs: register address offset from the IP block base address
 *
 * Read the contents of the IP block register located at offset @offs from the
 * IP block base, given a pointer @ssp to a struct shakti_serial_port record.
 *
 * Context: Any context.
 *
 * Returns: the value of the UART register
 */
static u32 shakti_readl(struct shakti_serial_port *ssp, u16 offs)
{
	while ((shakti_early_readl(&ssp->port, REG_STATUS) & STS_RX_NOT_EMPTY) == 0);
	return shakti_early_readl(&ssp->port, offs);
}

/**
 * __ssp_writel() - write to a Shakti serial port register
 * @v: value to write to the register
 * @offs: register address offset from the IP block base address
 * @ssp: pointer to a struct shakti_serial_port record
 *
 * Write the value @v to the IP block register located at offset @offs from the
 * IP block base, given a pointer @ssp to a struct shakti_serial_port record.
 *
 * Context: Any context.
 */
static void shakti_writel(u32 v, u16 offs, struct shakti_serial_port *ssp)
{
	
	while(shakti_readl(ssp, REG_STATUS) &  STS_TX_FULL);
	shakti_early_writel(v, offs, &ssp->port);
}

/**
 * shakti_serial_is_txfifo_full() - is the TXFIFO full?
 * @ssp: pointer to a struct shakti_serial_port
 *
 * Read the transmit FIFO "full" bit, returning a non-zero value if the
 * TX FIFO is full, or zero if space remains.  Intended to be used to prevent
 * writes to the TX FIFO when it's full.
 *
 * Returns: SHAKTI_SERIAL_TXDATA_FULL_MASK (non-zero) if the transmit FIFO
 * is full, or 0 if space remains.
 */
static int shakti_serial_is_txfifo_full(struct shakti_serial_port *ssp)
{
	// Need to rewrite.
	while(shakti_early_readl(&ssp->port, REG_STATUS)& STS_TX_FULL);
	return 0; 
}

/**
 * shakti_transmit_char() - enqueue a byte to transmit onto the TX FIFO
 * @ssp: pointer to a struct shakti_serial_port
 * @ch: character to transmit
 *
 * Enqueue a byte @ch onto the transmit FIFO, given a pointer @ssp to the
 * struct shakti_serial_port * to transmit on.  Caller should first check to
 * ensure that the TXFIFO has space; see shakti_serial_is_txfifo_full().
 *
 * Context: Any context.
 */
static void shakti_transmit_char(struct shakti_serial_port *ssp, int ch)
{
	shakti_writel(ch, REG_TX, ssp);
}

static unsigned int shakti_uart_tx_empty(struct uart_port *port)
{
	return (shakti_early_readl(port, REG_STATUS) & 0x1) ? TIOCSER_TEMT : 0; 
}

static void shakti_uart_set_mctrl(struct uart_port *port, unsigned int mctrl)
{
	/* IP block does not support these modem signals */
}

static unsigned int shakti_uart_get_mctrl(struct uart_port *port)
{
	/* IP block does not support these modem signals */
	return 0;
}

static void shakti_uart_stop_tx(struct uart_port *port)
{
	struct shakti_serial_port *ssp = port_to_shakti_serial_port(port);

	// Disable interrupts watermark(If being used)
}

static void shakti_write_one(struct shakti_serial_port *ssp, u32 value,
				unsigned int count)
{
}

// static void shakti_write(struct shakti_serial_port *ssp, const char *s,
// 			    unsigned int count)
// {
	// unsigned int written = 0, i = 0;
	// bool insert_nl = false;
	// u32 value = 0;

	// while (i < count) {
	// 	if (insert_nl) {
	// 		value |= TCU_MBOX_BYTE(written++, '\n');
	// 		insert_nl = false;
	// 		i++;
	// 	} else if (s[i] == '\n') {
	// 		value |= TCU_MBOX_BYTE(written++, '\r');
	// 		insert_nl = true;
	// 	} else {
	// 		value |= TCU_MBOX_BYTE(written++, s[i++]);
	// 	}

	// 	if (written == 3) {
	// 		tegra_tcu_write_one(tcu, value, 3);
	// 		value = written = 0;
	// 	}
	// }

	// if (written)
	// 	tegra_tcu_write_one(tcu, value, written);
// }

static void shakti_uart_start_tx(struct uart_port *port)
{
	struct shakti_serial_port *ssp = port_to_shakti_serial_port(port);

	//  Enable TX Interrupts Watermark(if needed)

	// struct tegra_tcu *tcu = port->private_data;
	// struct circ_buf *xmit = &port->state->xmit;
	// unsigned long count;

	// for (;;) {
	// 	count = CIRC_CNT_TO_END(xmit->head, xmit->tail, UART_XMIT_SIZE);
	// 	if (!count)
	// 		break;

	// 	tegra_tcu_write(tcu, &xmit->buf[xmit->tail], count);
	// 	xmit->tail = (xmit->tail + count) & (UART_XMIT_SIZE - 1);
	// }

	// uart_write_wakeup(port);
}

static void shakti_uart_stop_rx(struct uart_port *port)
{
	struct shakti_serial_port *ssp = port_to_shakti_serial_port(port);

	// Disable RX Interrupt Watermark(if needed)
}

static void shakti_uart_break_ctl(struct uart_port *port, int ctl)
{
	/* IP block does not support sending a break */
}

static int shakti_uart_startup(struct uart_port *port)
{
	struct shakti_serial_port *ssp = port_to_shakti_serial_port(port);

	/* Enable all related registers and interrupts for the operation
	   to start and UART IP to start working */

	return 0;
}

static void shakti_uart_shutdown(struct uart_port *port)
{
	struct shakti_serial_port *ssp = port_to_shakti_serial_port(port);

	/* Disable all the interrupts and registers which are to be resetted */
}

static void shakti_uart_set_termios(struct uart_port *port,
				       struct ktermios *new,
				       struct ktermios *old)
{

}

static int shakti_serial_verify_port(struct uart_port *port,
				     struct serial_struct *ser)
{
	return -EINVAL;
}

static const char *shakti_serial_type(struct uart_port *port)
{
	return port->type == SHAKTI_PORT_TYPE0 ? "Shakti UART 0" : NULL;
}

// static int shakti_uart_poll_get_char(struct uart_port *port)
// {
// 	int c;
// 	unsigned long flags;

// 	spin_lock_irqsave(&port->lock, flags);

// 	/* Check if FIFO is empty */
// 	if (shakti_early_readl(port, REG_STATUS)&0x4)
// 		c = (unsigned char) shakti_readl(port_to_shakti_serial_port(port), REG_RX);
// 	else /* Read a character */
// 		c = NO_POLL_CHAR;

// 	spin_unlock_irqrestore(&port->lock, flags);

// 	return c;
// }

// static void shakti_uart_poll_put_char(struct uart_port *port, unsigned char c)
// {
// 	unsigned long flags;

// 	spin_lock_irqsave(&port->lock, flags);

// 	/* Wait until FIFO is empty */
// 	while (!shakti_early_readl(port, REG_STATUS)&0x1)
// 		cpu_relax();

// 	/* Write a character */
// 	shakti_writel(c, REG_TX, port_to_shakti_serial_port(port));

// 	/* Wait until FIFO is empty */
// 	while (!shakti_early_readl(port, REG_STATUS)&0x1)
// 		cpu_relax();

// 	spin_unlock_irqrestore(&port->lock, flags);
// }

static const struct uart_ops shakti_uart_ops = {
	.tx_empty = shakti_uart_tx_empty,
	.set_mctrl = shakti_uart_set_mctrl,
	.get_mctrl = shakti_uart_get_mctrl,
	.stop_tx = shakti_uart_stop_tx,
	.start_tx = shakti_uart_start_tx,
	.stop_rx = shakti_uart_stop_rx,
	.break_ctl = shakti_uart_break_ctl,
	.startup = shakti_uart_startup,
	.shutdown = shakti_uart_shutdown,
	.set_termios = shakti_uart_set_termios,
	// .poll_init = shakti_uart_poll_init,
	// .poll_put_char = shakti_uart_poll_put_char,
	// .poll_get_char = shakti_uart_poll_get_char,
	.type		= shakti_serial_type,
};

static struct shakti_serial_port *shakti_serial_console_ports[SHAKTI_SERIAL_MAX_PORTS];

static int __init shakti_serial_console_setup(struct console *co, char *options)
{
	struct shakti_serial_port *ssp;
	int baud = SHAKTI_DEFAULT_BAUD_RATE;
	int bits = 16;
	int parity = 'n';
	int flow = 'n';

	if (co->index < 0 || co->index >= SHAKTI_SERIAL_MAX_PORTS)
		return -ENODEV;

	ssp = shakti_serial_console_ports[co->index];
	if (!ssp)
		return -ENODEV;

	if (options)
		uart_parse_options(options, &baud, &parity, &bits, &flow);

	return uart_set_options(&ssp->port, co, baud, parity, bits, flow);
}

/**
 * __ssp_wait_for_xmitr() - wait for an empty slot on the TX FIFO
 * @ssp: pointer to a struct shakti_serial_port
 *
 * Delay while the UART TX FIFO referred to by @ssp is marked as full.
 *
 * Context: Any context.
 */
static void __maybe_unused shakti_wait_for_xmitr(struct shakti_serial_port *ssp)
{
	shakti_serial_is_txfifo_full(ssp);
		// udelay(1); /* XXX Could probably be more intelligent here */
}

static void shakti_serial_console_putchar(struct uart_port *port, int ch)
{
	struct shakti_serial_port *ssp = port_to_shakti_serial_port(port);

	// shakti_wait_for_xmitr(ssp);
	shakti_transmit_char(ssp, ch);
}


void shakti_serial_console_write(struct console *co, const char *s, unsigned int count){

	struct shakti_serial_port *ssp = shakti_serial_console_ports[co->index];
	unsigned long flags;
	unsigned int ier;
	int locked = 1;

	if (!ssp)
		return;

	local_irq_save(flags);
	if (ssp->port.sysrq)
		locked = 0;
	else if (oops_in_progress)
		locked = spin_trylock(&ssp->port.lock);
	else
		spin_lock(&ssp->port.lock);

	ier = shakti_early_readl(&ssp->port, REG_INT_EN);
	shakti_early_writel(0, REG_INT_EN, &ssp->port);

	// writel_relaxed('A',ssp->port.membase + 0x4); // To test the UART TX FIFO...
	uart_console_write(&ssp->port, s, count, shakti_serial_console_putchar);

	shakti_early_writel(ier, REG_INT_EN, &ssp->port);

	if (locked)
		spin_unlock(&ssp->port.lock);
	local_irq_restore(flags);
}

int shakti_serial_console_read(struct console *co, char *s, unsigned int count){
	struct shakti_serial_port *ssp = shakti_serial_console_ports[co->index];
	int ret = 0;

	writel_relaxed('B',ssp->port.membase + 0x4); // To test the UART TX FIFO...
	shakti_readl(ssp, REG_RX);
	return ret;
}

static struct uart_driver shakti_serial_uart_driver;

static struct console shakti_serial_console = {
	.name		= SHAKTI_TTY_PREFIX,
	.write		= shakti_serial_console_write,
	.read       = shakti_serial_console_read,
	.device		= uart_console_device,
	.setup		= shakti_serial_console_setup,
	.flags		= CON_PRINTBUFFER,
	.index		= -1,
	.data		= &shakti_serial_uart_driver,
};

#define SHAKTI_SERIAL_CONSOLE	(&shakti_serial_console)

static struct uart_driver shakti_serial_uart_driver = {
	.owner		= THIS_MODULE,
	.driver_name	= SHAKTI_SERIAL_NAME,
	.dev_name	= SHAKTI_TTY_PREFIX,
	.nr		= SHAKTI_SERIAL_MAX_PORTS,
	.cons		= SHAKTI_SERIAL_CONSOLE,
};

static int __init shakti_console_init(void)
{
	register_console(&shakti_serial_console);
	return 0;
}

console_initcall(shakti_console_init);

static void shakti_add_console_port(struct shakti_serial_port *ssp)
{
	shakti_serial_console_ports[ssp->port.line] = ssp;
}

static void shakti_remove_console_port(struct shakti_serial_port *ssp)
{
	shakti_serial_console_ports[ssp->port.line] = 0;
}

/**
 * shakti_update_div() - calculate the divisor setting by the line rate
 * @ssp: pointer to a struct shakti_serial_port
 *
 * Calculate the appropriate value of the clock divisor for the UART
 * and target line rate referred to by @ssp and write it into the
 * hardware.
 */
static void shakti_update_div(struct shakti_serial_port *ssp)
{
	u16 div;

	div = DIV_ROUND_UP(ssp->clkin_rate, 16*(ssp->baud_rate)) - 1;
	printk(KERN_ALERT "THe div is %d\n",div);
	// THe below line is the place where the stuck is present...
	// shakti_writel(div, REG_BAUD, ssp);
	shakti_early_writel(div, REG_BAUD, &ssp->port);
	printk(KERN_ALERT "Baud rate set....\n");
}

static irqreturn_t shakti_serial_irq(int irq, void *dev_id)
{
	struct shakti_serial_port *ssp = dev_id;
	u32 ip;

	spin_lock(&ssp->port.lock);

	// ip = __ssp_readl(ssp, SIFIVE_SERIAL_IP_OFFS);
	// if (!ip) {
	// 	spin_unlock(&ssp->port.lock);
	// 	return IRQ_NONE;
	// }
	writel_relaxed('C',ssp->port.membase + 0x4); // To test the UART TX FIFO...
	printk(KERN_ALERT "This is a interrupt test...\n");

	// if (ip & SIFIVE_SERIAL_IP_RXWM_MASK)
	// 	__ssp_receive_chars(ssp);
	// if (ip & SIFIVE_SERIAL_IP_TXWM_MASK)
	// 	__ssp_transmit_chars(ssp);

	spin_unlock(&ssp->port.lock);

	return IRQ_HANDLED;
}

/**
 * shakti_show_registers - Show the main register values at the calling point...
 */
static void shakti_show_registers(struct shakti_serial_port *ssp)
{
	// #ifdef SHAKTI_DEBUG
	printk(KERN_ALERT "[SHAKTI-INFO] The UART Status Register has the following value : %x\n",shakti_early_readl(&ssp->port, REG_STATUS));
	printk(KERN_ALERT "[SHAKTI-INFO] The UART Control Register has the following value : %x\n",shakti_early_readl(&ssp->port, REG_CONTROL));
	printk(KERN_ALERT "[SHAKTI-INFO] The UART IEN Register has the following value : %x\n",shakti_early_readl(&ssp->port, REG_INT_EN));
	printk(KERN_ALERT "[SHAKTI-INFO] The UART Delay Register has the following value : %x\n",shakti_early_readl(&ssp->port, REG_DELAY));
	printk(KERN_ALERT "[SHAKTI-INFO] The UART IQCycles Register has the following value : %x\n",shakti_early_readl(&ssp->port, REG_IQ_CYCLES));
	printk(KERN_ALERT "[SHAKTI-INFO] The UART RX Threshold Register has the following value : %x\n",shakti_early_readl(&ssp->port, REG_RX_THRES));
	// #endif
}

static int shakti_uart_probe(struct platform_device *pdev)
{
	printk(KERN_ALERT "Shakti UART Probing started...\n");
	struct shakti_serial_port *ssp;
	struct resource *mem;
	struct clk *clk;
	void __iomem *base;
	int irq, id, r, rc;

	// irq = platform_get_irq(pdev, 0);
	// if (irq < 0)
	// 	return -EPROBE_DEFER;
	// asm volatile("ebreak");

	void __iomem *pinmux_register = ioremap(PINMUX_CONFIGURE_REG, 4);

	// Writing the pinmux register value for SPI...
	u32 pinmux_reg_read = 0;//( readl_relaxed(pinmux_register) | SHAKTI_SPI_PINMUX_MASK );
	writel_relaxed((SHAKTI_SPI_PINMUX_SET | pinmux_reg_read) ,pinmux_register);
	// printf("Hello...\n");
	printk(KERN_ALERT "Pinmux setting set and the device memory allocation started...\n");

	mem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	base = devm_ioremap_resource(&pdev->dev, mem);
	if (IS_ERR(base)) {
		dev_err(&pdev->dev, "could not acquire device memory\n");
		return PTR_ERR(base);
	}
	// writel_relaxed('A',base + 0x4); // To test the UART TX FIFO...

	// clk = devm_clk_get(&pdev->dev, NULL);
	// printk(KERN_ALERT "The clock is being set\n");
	// printk(KERN_ALERT "The clock rate is %d\n",clk_get_rate(clk));
	// if (IS_ERR(clk)) {
	// 	dev_err(&pdev->dev, "unable to find controller clock\n");
	// 	return PTR_ERR(clk);
	// }

	// char *clk_success = "Clock accquired Successful \n";
	// uart_console_write(&ssp->port, clk_success, 28, shakti_serial_console_putchar);

	// id = of_alias_get_id(pdev->dev.of_node, "serial");
	// if (id < 0) {
	// 	dev_err(&pdev->dev, "missing aliases entry\n");
	// 	return id;
	// }

// #ifdef CONFIG_SERIAL_SHAKTI_CONSOLE
	// if (id > SHAKTI_SERIAL_MAX_PORTS) {
	// 	dev_err(&pdev->dev, "too many UARTs (%d)\n", id);
	// 	return -EINVAL;
	// }
// #endif
	// Missing in the previous version of the driver...
	// rc = uart_register_driver(&shakti_serial_uart_driver);
	// // return_sts = rc;
	// printk(KERN_ALERT "Trying to register the UART... %d\n",rc);
	// if (rc < 0) {
	// 	dev_err(&pdev->dev, "Failed to register driver\n");
	// 	printk(KERN_ALERT "Failed to register Driver...\n");
	// 	goto probe_out1;
	// }
	// printk(KERN_ALERT "UART Device registered with Core Successfully...\n");

	// char *mem_success = "Registered Successful \n";
	// uart_console_write(&ssp->port, mem_success, 29, shakti_serial_console_putchar);
	ssp = devm_kzalloc(&pdev->dev, sizeof(*ssp), GFP_KERNEL);
	if (!ssp)
		return -ENOMEM;

	ssp->port.dev = &pdev->dev;
	ssp->port.type = SHAKTI_PORT_TYPE0;
	ssp->port.iotype = UPIO_MEM;
	// ssp->port.irq = irq;
	ssp->port.fifosize = SHAKTI_TX_FIFO_DEPTH;
	ssp->port.ops = &shakti_uart_ops;
	ssp->port.line = id;
	ssp->port.mapbase = mem->start;
	ssp->port.membase = base;
	// ssp->port.flags = UPF_BOOT_AUTOCONF;  //Added after checking imx.c which had this as standard console...
	ssp->dev = &pdev->dev;
	ssp->clk = clk;

	printk(KERN_ALERT "Setup done need to set baud rate...\n");
	// ssp->clk_notifier.notifier_call = shakti_serial_clk_notifier;

	// r = clk_notifier_register(ssp->clk, &ssp->clk_notifier);
	// if (r) {
	// 	dev_err(&pdev->dev, "could not register clock notifier: %d\n",
	// 		r);
	// 	goto probe_out1;
	// }

	/* Set up clock divider */
	ssp->clkin_rate = 50000000;//clk_get_rate(ssp->clk);
	// int ret = of_property_read_u32(pdev->dev.of_node, "shakti,baud-rate", &ssp->baud_rate);
	// if (ret < 0)
	ssp->baud_rate = SHAKTI_DEFAULT_BAUD_RATE;
	printk(KERN_ALERT "Just before setting baud rate...\n");
	shakti_update_div(ssp);
	platform_set_drvdata(pdev, ssp);

	// /* Enable transmits and set the watermark level to 1 */
	// __ssp_writel((1 << SHAKTI_SERIAL_TXCTRL_TXCNT_SHIFT) |
	// 	     SHAKTI_SERIAL_TXCTRL_TXEN_MASK,
	// 	     SHAKTI_SERIAL_TXCTRL_OFFS, ssp);

	// /* Enable receives and set the watermark level to 0 */
	// __ssp_writel((0 << SHAKTI_SERIAL_RXCTRL_RXCNT_SHIFT) |
	// 	     SHAKTI_SERIAL_RXCTRL_RXEN_MASK,
	// 	     SHAKTI_SERIAL_RXCTRL_OFFS, ssp);

	// r = request_irq(ssp->port.irq, shakti_serial_irq, ssp->port.irqflags,
	// 		dev_name(&pdev->dev), ssp);
	// if (r) {
	// 	dev_err(&pdev->dev, "could not attach interrupt: %d\n", r);
	// 	goto probe_out2;
	// }

	// u8 ier = shakti_early_readl(&ssp->port, REG_INT_EN);
	// ier|=0x4;
	// shakti_early_writel(ier, REG_INT_EN, &ssp->port);

	shakti_add_console_port(ssp);

	r = uart_add_one_port(&shakti_serial_uart_driver, &ssp->port);
	if (r != 0) {
		dev_err(&pdev->dev, "could not add uart: %d\n", r);
		goto probe_out3;
	}

	printk(KERN_ALERT "Setting the LED high...\n");
	void __iomem *gpio_base = ioremap(GPIO_ADDR, 8);
	u32 status = readl_relaxed(gpio_base + 0x00);
	status |=0x8;
	writel_relaxed(status, gpio_base + 0x00);

	writel_relaxed(0x8, gpio_base + 0x4); //High GPIO 4.
	shakti_show_registers(ssp);
	return 0;

probe_out3:
	shakti_remove_console_port(ssp);
probe_out2:
	// clk_notifier_unregister(ssp->clk, &ssp->clk_notifier);
	// free_irq(ssp->port.irq, ssp);  //Interrupt free after probe failure...
probe_out1:
	uart_unregister_driver(&shakti_serial_uart_driver);

	return r;
}
static int shakti_uart_remove(struct platform_device *pdev)
{
	struct shakti_serial_port *ssp = platform_get_drvdata(pdev);

	shakti_remove_console_port(ssp);
	// unregister_console(&ssp->console);
	uart_remove_one_port(&shakti_serial_uart_driver, &ssp->port);
	// uart_unregister_driver(&shakti_serial_uart_driver);

	return 0;
}

static const struct of_device_id shakti_uart_match[] = {
	{ .compatible = "shakti,uart0" },
	{ }
};

static struct platform_driver shakti_uart_driver = {
	.driver = {
		.name = "shakti-uart",
		.of_match_table = shakti_uart_match,
	},
	.probe = shakti_uart_probe,
	.remove = shakti_uart_remove,
};
module_platform_driver(shakti_uart_driver);

static int __init shakti_uart_init(void)
{

	int r;

	r = uart_register_driver(&shakti_serial_uart_driver);
	if (r)
		goto init_out1;

	r = platform_driver_register(&shakti_uart_driver);
	if (r)
		goto init_out2;

	return 0;

init_out2:
	uart_unregister_driver(&shakti_serial_uart_driver);
init_out1:
	return r;
	// /* Register the platform driver */
	// return platform_driver_register(&shakti_uart_driver);
}

static void __exit shakti_uart_exit(void)
{
	/* Unregister the platform driver */
	platform_driver_unregister(&shakti_uart_driver);
	uart_unregister_driver(&shakti_serial_uart_driver);
}

arch_initcall(shakti_uart_init);
module_exit(shakti_uart_exit);

MODULE_AUTHOR("Venkatakrishnan Sutharsan <venkatakrishnan.sutharsan@gmail.com>");
MODULE_LICENSE("GPL v2");
MODULE_DESCRIPTION("Shakti UART Driver");
